<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2012 Jan <janYYYY@t3easy.de>
 *  			
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Test case for class Tx_T3seeTopics_Domain_Model_Topic.
 *
 * @version $Id$
 * @copyright Copyright belongs to the respective authors
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 * @package TYPO3
 * @subpackage T3see topics
 *
 * @author Jan <janYYYY@t3easy.de>
 */
class Tx_T3seeTopics_Domain_Model_TopicTest extends Tx_Extbase_Tests_Unit_BaseTestCase {
	/**
	 * @var Tx_T3seeTopics_Domain_Model_Topic
	 */
	protected $fixture;

	public function setUp() {
		$this->fixture = new Tx_T3seeTopics_Domain_Model_Topic();
	}

	public function tearDown() {
		unset($this->fixture);
	}

	/**
	 * @test
	 */
	public function getTitleReturnsInitialValueForString() { }

	/**
	 * @test
	 */
	public function setTitleForStringSetsTitle() { 
		$this->fixture->setTitle('Conceived at T3CON10');

		$this->assertSame(
			'Conceived at T3CON10',
			$this->fixture->getTitle()
		);
	}

    /**
     * @test
     */
    public function getAuthorNameReturnsInitialValueForString() { }

    /**
     * @test
     */
    public function setAuthorNameForStringSetsAuthorName() {
        $this->fixture->setAuthorName('Jan Kiesewetter');

        $this->assertSame(
            'Jan Kiesewetter',
            $this->fixture->getAuthorName()
        );
    }

	/**
	 * @test
	 */
	public function getAuthorMailReturnsInitialValueForString() { }

	/**
	 * @test
	 */
	public function setAuthorMailForStringSetsAuthorMail() {
		$this->fixture->setAuthorMail('janYYYY@kwetter.de');

		$this->assertSame(
			'janYYYY@kwetter.de',
			$this->fixture->getAuthorMail()
		);
	}

    /**
     * @test
     */
    public function getScoreReturnsInitialValueForScore() {
        $this->assertSame(
            0,
            $this->fixture->getScore()
        );
    }

    /**
     * @test
     */
    public function setScoreForIntegerSetsScore() {
        $this->fixture->setScore(12);

        $this->assertSame(
            12,
            $this->fixture->getScore()
        );
    }

    /**
     * @test
     */
    public function getTokenReturnsInitialValueForString() { }

    /**
     * @test
     */
    public function setTokenForStringSetsToken() {
        $this->fixture->settoken('Conceived at T3CON10');

        $this->assertSame(
            'Conceived at T3CON10',
            $this->fixture->getToken()
        );
    }

    /**
     * @test
     */
    public function getCrdateReturnsInitialValueForCrdate() { }

    /**
     * @test
     */
    public function setCrdateForDateTimeSetsCrdate() {
        $dateTime = New DateTime();

        $this->fixture->setCrdate($dateTime);

        $this->assertSame(
            $dateTime,
            $this->fixture->getCrdate()
        );
    }

    /**
     * @test
     */
    public function getHiddenReturnsInitialValueForHidden() {
        $default = FALSE;
        $this->assertSame (
            $default,
            $this->fixture->getHidden()
        );
    }

    /**
     * @test
     */
    public function setHiddenForBoolSetsHidden() {

        $this->fixture->setHidden(TRUE);

        $this->assertSame(
            TRUE,
            $this->fixture->getHidden()
        );
    }

    /**
     * @test
     */
    public function getDeletedReturnsInitialValueForDeleted() {
        $default = FALSE;
        $this->assertSame (
            $default,
            $this->fixture->getDeleted()
        );
    }

    /**
     * @test
     */
    public function setDeletedForBoolSetsDeleted() {

        $this->fixture->setDeleted(TRUE);

        $this->assertSame(
            TRUE,
            $this->fixture->getDeleted()
        );
    }

    /**
	 * @test
	 */
	public function getDescriptionReturnsInitialValueForString() { }

	/**
	 * @test
	 */
	public function setDescriptionForStringSetsDescription() { 
		$this->fixture->setDescription('Conceived at T3CON10');

		$this->assertSame(
			'Conceived at T3CON10',
			$this->fixture->getDescription()
		);
	}
	
	/**
	 * @test
	 */
	public function getVotesReturnsInitialValueForObjectStorageContainingTx_T3seeTopics_Domain_Model_Vote() { 
		$newObjectStorage = new Tx_Extbase_Persistence_ObjectStorage();
		$this->assertEquals(
			$newObjectStorage,
			$this->fixture->getVotes()
		);
	}

	/**
	 * @test
	 */
	public function setVotesForObjectStorageContainingTx_T3seeTopics_Domain_Model_VoteSetsVotes() { 
		$vote = new Tx_T3seeTopics_Domain_Model_Vote();
		$objectStorageHoldingExactlyOneVotes = new Tx_Extbase_Persistence_ObjectStorage();
		$objectStorageHoldingExactlyOneVotes->attach($vote);
		$this->fixture->setVotes($objectStorageHoldingExactlyOneVotes);

		$this->assertSame(
			$objectStorageHoldingExactlyOneVotes,
			$this->fixture->getVotes()
		);
	}
	
	/**
	 * @test
	 */
	public function addVoteToObjectStorageHoldingVotes() {
		$vote = new Tx_T3seeTopics_Domain_Model_Vote();
		$objectStorageHoldingExactlyOneVote = new Tx_Extbase_Persistence_ObjectStorage();
		$objectStorageHoldingExactlyOneVote->attach($vote);
		$this->fixture->addVote($vote);

		$this->assertEquals(
			$objectStorageHoldingExactlyOneVote,
			$this->fixture->getVotes()
		);
	}

	/**
	 * @test
	 */
	public function removeVoteFromObjectStorageHoldingVotes() {
		$vote = new Tx_T3seeTopics_Domain_Model_Vote();
		$localObjectStorage = new Tx_Extbase_Persistence_ObjectStorage();
		$localObjectStorage->attach($vote);
		$localObjectStorage->detach($vote);
		$this->fixture->addVote($vote);
		$this->fixture->removeVote($vote);

		$this->assertEquals(
			$localObjectStorage,
			$this->fixture->getVotes()
		);
	}
	
}
?>