<?php
if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}

Tx_Extbase_Utility_Extension::registerPlugin(
	$_EXTKEY,
	'Topics',
	'Topics'
);

$pluginSignature = str_replace('_','',$_EXTKEY) . '_' . topics;
$TCA['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature] = 'pi_flexform';
t3lib_extMgm::addPiFlexFormValue($pluginSignature, 'FILE:EXT:' . $_EXTKEY . '/Configuration/FlexForms/flexform_' .topics. '.xml');

t3lib_extMgm::addStaticFile($_EXTKEY, 'Configuration/TypoScript', 'T3see topics');

t3lib_div::requireOnce(t3lib_extMgm::extPath($_EXTKEY) . 'Classes/Utility/TCAUserFunc.php');

t3lib_extMgm::addLLrefForTCAdescr('tx_t3seetopics_domain_model_topic', 'EXT:t3see_topics/Resources/Private/Language/locallang_csh_tx_t3seetopics_domain_model_topic.xml');
t3lib_extMgm::allowTableOnStandardPages('tx_t3seetopics_domain_model_topic');
$TCA['tx_t3seetopics_domain_model_topic'] = array(
	'ctrl' => array(
		'title'	=> 'LLL:EXT:t3see_topics/Resources/Private/Language/locallang_db.xml:tx_t3seetopics_domain_model_topic',
		'label' => 'title',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,
		'versioningWS' => 2,
		'versioning_followPages' => TRUE,
		'origUid' => 't3_origuid',
		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',
		'delete' => 'deleted',
		'enablecolumns' => array(
			'disabled' => 'hidden',
			'starttime' => 'starttime',
			'endtime' => 'endtime',
		),
		'dynamicConfigFile' => t3lib_extMgm::extPath($_EXTKEY) . 'Configuration/TCA/Topic.php',
		'iconfile' => t3lib_extMgm::extRelPath($_EXTKEY) . 'Resources/Public/Icons/tx_t3seetopics_domain_model_topic.gif'
	),
);

t3lib_extMgm::addLLrefForTCAdescr('tx_t3seetopics_domain_model_vote', 'EXT:t3see_topics/Resources/Private/Language/locallang_csh_tx_t3seetopics_domain_model_vote.xml');
t3lib_extMgm::allowTableOnStandardPages('tx_t3seetopics_domain_model_vote');
$TCA['tx_t3seetopics_domain_model_vote'] = array(
	'ctrl' => array(
		'title'	=> 'LLL:EXT:t3see_topics/Resources/Private/Language/locallang_db.xml:tx_t3seetopics_domain_model_vote',
		'label' => 'value',
		'label_userFunc' => 'Tx_T3seeTopics_Utility_TCAUserFunc->getVoteLabel',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,
		'delete' => 'deleted',
		'enablecolumns' => array(
			'disabled' => 'hidden',
		),
		'dynamicConfigFile' => t3lib_extMgm::extPath($_EXTKEY) . 'Configuration/TCA/Vote.php',
		'iconfile' => t3lib_extMgm::extRelPath($_EXTKEY) . 'Resources/Public/Icons/tx_t3seetopics_domain_model_vote.gif'
	),
);

?>