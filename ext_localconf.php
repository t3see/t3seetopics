<?php
if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}

Tx_Extbase_Utility_Extension::configurePlugin(
	$_EXTKEY,
	'Topics',
	array(
		'Topic' => 'list, show, new, create, plusVote, minusVote, reviewRelease, reviewDelete',
		
	),
	// non-cacheable actions
	array(
		'Topic' => 'create, plusVote, minusVote, reviewRelease, reviewDelete',		
	)
);

?>