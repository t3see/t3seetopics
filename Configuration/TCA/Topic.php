<?php
if (!defined ('TYPO3_MODE')) {
	die ('Access denied.');
}

$TCA['tx_t3seetopics_domain_model_topic'] = array(
	'ctrl' => $TCA['tx_t3seetopics_domain_model_topic']['ctrl'],
	'interface' => array(
		'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, title, author_name, author_mail, description, score, crdate, votes',
	),
	'types' => array(
		'1' => array('showitem' => 'sys_language_uid;;;;1-1-1, l10n_parent, l10n_diffsource, hidden;;1, title, author_name, author_mail, description, score, token, crdate, votes,--div--;LLL:EXT:cms/locallang_ttc.xml:tabs.access,starttime, endtime'),
	),
	'palettes' => array(
		'1' => array('showitem' => ''),
	),
	'columns' => array(
		'sys_language_uid' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.language',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'sys_language',
				'foreign_table_where' => 'ORDER BY sys_language.title',
				'items' => array(
					array('LLL:EXT:lang/locallang_general.xml:LGL.allLanguages', -1),
					array('LLL:EXT:lang/locallang_general.xml:LGL.default_value', 0)
				),
			),
		),
		'l10n_parent' => array(
			'displayCond' => 'FIELD:sys_language_uid:>:0',
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.l18n_parent',
			'config' => array(
				'type' => 'select',
				'items' => array(
					array('', 0),
				),
				'foreign_table' => 'tx_t3seetopics_domain_model_topic',
				'foreign_table_where' => 'AND tx_t3seetopics_domain_model_topic.pid=###CURRENT_PID### AND tx_t3seetopics_domain_model_topic.sys_language_uid IN (-1,0)',
			),
		),
		'l10n_diffsource' => array(
			'config' => array(
				'type' => 'passthrough',
			),
		),
		't3ver_label' => array(
			'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.versionLabel',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'max' => 255,
			)
		),
		'hidden' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.hidden',
			'config' => array(
				'type' => 'check',
			),
		),
		'deleted' => array(
			'config' => array(
				'type' => 'passthrough',
			),
		),
		'starttime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.starttime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),
		'endtime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.endtime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),
		'title' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:t3see_topics/Resources/Private/Language/locallang_db.xml:tx_t3seetopics_domain_model_topic.title',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim,required'
			),
		),
		'author_name' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:t3see_topics/Resources/Private/Language/locallang_db.xml:tx_t3seetopics_domain_model_topic.author_name',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'author_mail' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:t3see_topics/Resources/Private/Language/locallang_db.xml:tx_t3seetopics_domain_model_topic.author_mail',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim,required'
			),
		),
		'description' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:t3see_topics/Resources/Private/Language/locallang_db.xml:tx_t3seetopics_domain_model_topic.description',
			'config' => array(
				'type' => 'text',
				'cols' => 48,
				'rows' => 5,
				'wizards' => array(
					'RTE' => array(
						'icon' => 'wizard_rte2.gif',
						'notNewRecords'=> 1,
						'RTEonly' => 1,
						'script' => 'wizard_rte.php',
						'title' => 'LLL:EXT:cms/locallang_ttc.xml:bodytext.W.RTE',
						'type' => 'script'
					)
				)
			),
			'defaultExtras' => 'richtext[]:rte_transform[mode=ts_css]',
		),
		'score' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:t3see_topics/Resources/Private/Language/locallang_db.xml:tx_t3seetopics_domain_model_topic.score',
			'config' => array(
				'type' => 'input',
				'size' => 4,
				'eval' => 'int',
				'readOnly' => 1,
			),
		),
		'token' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:t3see_topics/Resources/Private/Language/locallang_db.xml:tx_t3seetopics_domain_model_topic.token',
			'config' => array(
				'type' => 'input',
				'readOnly' => TRUE,
			),
		),
		'crdate' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:t3see_topics/Resources/Private/Language/locallang_db.xml:tx_t3seetopics_domain_model_topic.crdate',
			'config' => array(
				'type' => 'none',
				'format' => 'datetime',
                'eval' => 'datetime',
			),
		),
		'votes' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:t3see_topics/Resources/Private/Language/locallang_db.xml:tx_t3seetopics_domain_model_topic.votes',
			'config' => array(
				'type' => 'inline',
				'foreign_table' => 'tx_t3seetopics_domain_model_vote',
				'foreign_field' => 'topic',
				'maxitems'      => 9999,
				'appearance' => array(
					'collapse' => 0,
					'levelLinksPosition' => 'top',
					'showSynchronizationLink' => 1,
					'showPossibleLocalizationRecords' => 1,
					'showAllLocalizationLink' => 1
				),
			),
		),
	),
);

?>