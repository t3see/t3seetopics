<?php
if (!defined ('TYPO3_MODE')) {
	die ('Access denied.');
}

$TCA['tx_t3seetopics_domain_model_vote'] = array(
	'ctrl' => $TCA['tx_t3seetopics_domain_model_vote']['ctrl'],
	'interface' => array(
		'showRecordFieldList' => 'hidden, value',
	),
	'types' => array(
		'1' => array('showitem' => 'hidden;;1, value'),
	),
	'palettes' => array(
		'1' => array('showitem' => ''),
	),
	'columns' => array(
		'hidden' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.hidden',
			'config' => array(
				'type' => 'check',
			),
		),
		'value' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:t3see_topics/Resources/Private/Language/locallang_db.xml:tx_t3seetopics_domain_model_vote.value',
			'config' => array(
				'type' => 'input',
				'size' => 4,
				'eval' => 'int'
			),
		),
		'topic' => array(
			'config' => array(
				'type' => 'passthrough',
			),
		),
	),
);

?>