<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2012 Jan <janYYYY@t3easy.de>
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package t3see_topics
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class Tx_T3seeTopics_Controller_TopicController extends Tx_Extbase_MVC_Controller_ActionController {

	/**
	 * topicRepository
	 *
	 * @var Tx_T3seeTopics_Domain_Repository_TopicRepository
	 */
	protected $topicRepository;

	/**
	 * injectTopicRepository
	 *
	 * @param Tx_T3seeTopics_Domain_Repository_TopicRepository $topicRepository
	 * @return void
	 */
	public function injectTopicRepository(Tx_T3seeTopics_Domain_Repository_TopicRepository $topicRepository) {
		$this->topicRepository = $topicRepository;
	}

	public function initializeAction() {
		parent::initializeAction();
		$defaultHeaderType = intval($this->settings['defaultHeaderType']);
		$this->settings['subHeaderType'] = $defaultHeaderType + 1;	
	}

	/**
	 * action list
	 *
	 * @return void
	 */
	public function listAction() {
		$topics = $this->topicRepository->findAll();
		$this->view->assign('topics', $topics);
	}

	/**
	 * action show
	 *
	 * @param $topic
	 * @return void
	 */
	public function showAction(Tx_T3seeTopics_Domain_Model_Topic $topic) {
		Tx_Flextend_Service_ExtendTitle::extendTitle($topic->getTitle());
		$this->view->assign('topic', $topic);
	}

	/**
	 * action new
	 *
	 * @param $newTopic
	 * @dontvalidate $newTopic
	 * @return void
	 */
	public function newAction(Tx_T3seeTopics_Domain_Model_Topic $newTopic = NULL) {
		$this->view->assign('newTopic', $newTopic);
	}

	/**
	 * action create
	 *
	 * @param $newTopic
	 * @return void
	 */
	public function createAction(Tx_T3seeTopics_Domain_Model_Topic $newTopic) {
        $flashMessages = array();
		if ($this->settings['frontendCreate']) {
			
			# Feature admin review
			if($this->settings['reviewEmailAddress']) {
				
				$newTopic = $this->adminReview($newTopic);

				$subject = Tx_Extbase_Utility_Localization::translate(
					'tx_t3seetopics_domain_model_topic.reviewEmailSubject',
					'T3seeTopics',
                    array($newTopic->getTitle())
				);

				$mail = $this->objectManager->get('Tx_Flextend_Service_Email');

				$mail->setControllerName($this->request->getControllerName());
				$mail->setTemplateName('Email');

                $mailIsSend = $mail->send($this->settings['reviewEmailAddress'], $subject, array('topic' => $newTopic, 'dateTime' => new DateTime()));

                if ($mailIsSend) {
                    $flashMessages[] = Tx_Extbase_Utility_Localization::translate(
                        'tx_t3seetopics_domain_model_topic.review',
                        'T3seeTopics',
                        array($newTopic->getTitle())
                    );
                } else {
                    $flashMessages[] = Tx_Extbase_Utility_Localization::translate(
                        'tx_t3seetopics_domain_model_topic.reviewEmailError',
                        'T3seeTopics'
                    );
                }
			} else {
                $flashMessages[] = Tx_Extbase_Utility_Localization::translate(
                    'tx_t3seetopics_domain_model_topic.created',
                    'T3seeTopics',
                    array($newTopic->getTitle())
                );
            }
			
			$this->topicRepository->add($newTopic);
		} else {
			$flashMessages[] = Tx_Extbase_Utility_Localization::translate(
				'tx_t3seetopics_domain_model_topic.frontendCreateDisabled',
				'T3seeTopics'
			);
		}
        foreach ($flashMessages as $flashMessage) {
            $this->flashMessageContainer->add($flashMessage);
        }
		$this->redirect('list');
	}

	/**
	 * action plusVote
	 *
	 * @param Tx_T3seeTopics_Domain_Model_Topic $topic
	 * @return void
	 */
	public function plusVoteAction(Tx_T3seeTopics_Domain_Model_Topic $topic) {
		$this->vote($topic, 1);
		$this->redirect('list');
	}

	/**
	 * action minusVote
	 * 
	 * @param Tx_T3seeTopics_Domain_Model_Topic $topic
	 * @return void
	 */
	public function minusVoteAction(Tx_T3seeTopics_Domain_Model_Topic $topic) {
		$this->vote($topic, -1);
		$this->redirect('list');
	}
	
	/**
	 * private vote function
	 *
	 * @param Tx_T3seeTopics_Domain_Model_Topic $topic
	 * @param integer $value
	 * @return void
	 */
	private function vote(Tx_T3seeTopics_Domain_Model_Topic $topic, $value) {
		
		$ses = $GLOBALS['TSFE']->fe_user->getKey('ses','tx_t3seetopics');
		
		if ($ses['lastVote'] + $this->settings['secondsBetweenVotes'] < time()) {
			$vote = $this->objectManager->create('Tx_T3seeTopics_Domain_Model_Vote');
			$vote->setValue($value);
			$topic->addVote($vote);
			$topic->setScore($topic->getVoteScore());
			$this->topicRepository->update($topic);
			
			$flashMessage = Tx_Extbase_Utility_Localization::translate(
				'tx_t3seetopics_domain_model_vote.thanks',
				'T3seeTopics',
				array($topic->getTitle())
			);
			$this->flashMessageContainer->add($flashMessage);
			
			$myConfig = array();
			$myConfig['lastVote'] = time();
			$myConfig['votedTopics'] = $ses['votedTopics'];
			$myConfig['votedTopics'][] = $topic->getUid();
			$GLOBALS['TSFE']->fe_user->setKey('ses','tx_t3seetopics', $myConfig);
		} else {
			$timeUntilNextVote = date('i:s', $this->settings['secondsBetweenVotes'] - (time() - $ses['lastVote']));
			$flashMessage = Tx_Extbase_Utility_Localization::translate(
				'tx_t3seetopics_domain_model_vote.pleaseWait',
				'T3seeTopics',
				array($timeUntilNextVote)
			);
			$this->flashMessageContainer->add($flashMessage);
		}
	}
	
	/**
	 * Release the topic after review
	 * 
	 * @param string $token 
	 */
	public function reviewReleaseAction($token) {
		$topic = $this->topicRepository->findHiddenByToken($token);
		if ($topic instanceof Tx_T3seeTopics_Domain_Model_Topic) {
			
			if ($topic->getDeleted() === TRUE) {
				$flashMessage = Tx_Extbase_Utility_Localization::translate(
					'tx_t3seetopics_domain_model_topic.reviewAlreadyDeleted',
					'T3seeTopics',
					array($topic->getTitle())
				);
			} elseif ($topic->getHidden() === TRUE) {
				$topic->setHidden(FALSE);
				$flashMessage = Tx_Extbase_Utility_Localization::translate(
					'tx_t3seetopics_domain_model_topic.reviewRelease',
					'T3seeTopics',
					array($topic->getTitle())
				);
			} else {
				$flashMessage = Tx_Extbase_Utility_Localization::translate(
					'tx_t3seetopics_domain_model_topic.reviewAlreadyReleased',
					'T3seeTopics',
					array($topic->getTitle())
				);
				$this->flashMessageContainer->add($flashMessage);
				$this->redirect('show', NULL, NULL, array('topic' => $topic));
			}
		} else {
			$flashMessage = Tx_Extbase_Utility_Localization::translate(
				'tx_t3seetopics_domain_model_topic.reviewNotFound',
				'T3seeTopics'
			);
		}
		$this->flashMessageContainer->add($flashMessage);
		$this->redirect('list');
	}

	/**
	 * Delete the topic after review
	 * 
	 * @param string $token 
	 */
	public function reviewDeleteAction($token) {
		$topic = $this->topicRepository->findHiddenByToken($token);
		if ($topic instanceof Tx_T3seeTopics_Domain_Model_Topic) {
            if ($topic->getHidden() === TRUE) {
                $flashMessage = Tx_Extbase_Utility_Localization::translate(
                    'tx_t3seetopics_domain_model_topic.reviewDelete',
                    'T3seeTopics',
                    array($topic->getTitle())
                );
                $topic->setDeleted(TRUE);
            } else {
                $flashMessage = Tx_Extbase_Utility_Localization::translate(
                    'tx_t3seetopics_domain_model_topic.reviewAlreadyReleased',
                    'T3seeTopics',
                    array($topic->getTitle())
                );
                $flashMessage .= ' ' . Tx_Extbase_Utility_Localization::translate(
                    'tx_t3seetopics_domain_model_topic.reviewDeleteBackend',
                    'T3seeTopics'
                );
                $this->flashMessageContainer->add($flashMessage);
                $this->redirect('show', NULL, NULL, array('topic' => $topic));
            }
		} else {
			$flashMessage = Tx_Extbase_Utility_Localization::translate(
				'tx_t3seetopics_domain_model_topic.reviewError',
				'T3seeTopics'
			);
		}
		$this->flashMessageContainer->add($flashMessage);
		$this->redirect('list');
	}

	/**
	 *
	 * @param Tx_T3seeTopics_Domain_Model_Topic $topic
	 * @return Tx_T3seeTopics_Domain_Model_Topic $topic
	 */
	private function adminReview(Tx_T3seeTopics_Domain_Model_Topic $topic) {
		
		$topic->setHidden(TRUE);
		$token = hash_hmac('sha256', microtime() . $topic->getTitle() . mt_rand(), Tx_Flextend_Service_T3ConfVars::getEncryptionKey());
		$topic->setToken($token);
		return $topic;
	}
}
?>