<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2012 Jan <janYYYY@t3easy.de>
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package t3see_topics
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class Tx_T3seeTopics_Domain_Repository_TopicRepository extends Tx_Extbase_Persistence_Repository {
	protected $defaultOrderings = array (
		'score' => Tx_Extbase_Persistence_QueryInterface::ORDER_DESCENDING,
		'title' => Tx_Extbase_Persistence_QueryInterface::ORDER_ASCENDING
	);
	/**
	 *
	 * @param string $token
	 * @return Tx_T3seeTopics_Domain_Model_Topic 
	 */
	public function findHiddenByToken($token) {
		$query = $this->createQuery();
		$query->getQuerySettings()->setRespectEnableFields(FALSE);
		$topic = $query
			->matching(
				$query->equals('token', $token)
			)
			->execute()->getFirst();
		return $topic;
	}
	
}
?>