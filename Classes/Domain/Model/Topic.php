<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2012 Jan <janYYYY@t3easy.de>
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package t3see_topics
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class Tx_T3seeTopics_Domain_Model_Topic extends Tx_Extbase_DomainObject_AbstractEntity {

	/**
	 * Title of the Topic
	 *
	 * @var string
	 * @validate NotEmpty, StringLength(minimum=3)
	 */
	protected $title;

	/**
	 * Author of the topic
	 *
	 * @var string author name
	 * @validate NotEmpty, StringLength(minimum=3)
	 */
	protected $authorName;

	/**
	 * @var string author mail
	 * @validate EmailAddress
	 * @validate Tx_Flextend_Validation_Validator_MxValidator
	 */
	protected $authorMail;

	/**
	 * Description of the topic
	 *
	 * @var string
	 */
	protected $description;
	
	/**
	 * Score of the topic
	 * 
	 * @var integer 
	 */
	protected $score;
	
	/**
	 * Voting status
	 * 
	 * @var boolean 
	 */
	protected $voted;

	/**
	 * Token for review
	 * 
	 * @var string
	 */
	protected $token;
	
	/**
	 *
	 * @var DateTime
	 */
	protected $crdate;
	
	/**
	 * Is the topic hidden
	 *
	 * @var boolean 
	 */
	protected $hidden;
	
	/**
	 * Is the topic deleted
	 * 
	 * @var boolean
	 */
	protected $deleted;
	/**
	 * Score from the votes
	 * 
	 * @var integer 
	 */
	protected $voteScore;
	
	/**
	 * Relation to votes
	 *
	 * @var Tx_Extbase_Persistence_ObjectStorage<Tx_T3seeTopics_Domain_Model_Vote>
	 * @lazy
	 * @cascade remove
	 */
	protected $votes;

	/**
	 * __construct
	 *
	 * @return void
	 */
	public function __construct() {
		//Do not remove the next line: It would break the functionality
		$this->initStorageObjects();
	}

	/**
	 * Initializes all Tx_Extbase_Persistence_ObjectStorage properties.
	 *
	 * @return void
	 */
	protected function initStorageObjects() {
		/**
		 * Do not modify this method!
		 * It will be rewritten on each save in the extension builder
		 * You may modify the constructor of this class instead
		 */
		$this->votes = new Tx_Extbase_Persistence_ObjectStorage();
	}

	/**
	 * Returns the title
	 *
	 * @return string $title
	 */
	public function getTitle() {
		return $this->title;
	}

	/**
	 * Sets the title
	 *
	 * @param string $title
	 * @return void
	 */
	public function setTitle($title) {
		$this->title = $title;
	}

	/**
	 * Returns the description
	 *
	 * @return string $description
	 */
	public function getDescription() {
		return $this->description;
	}

	/**
	 * Sets the description
	 *
	 * @param string $description
	 * @return void
	 */
	public function setDescription($description) {
		$this->description = $description;
	}

	/**
	 * @return string
	 */
	public function getAuthorName() {
		return $this->authorName;
	}

	/**
	 * @param string $authorName
	 */
	public function setAuthorName($authorName) {
		$this->authorName = $authorName;
	}

	/**
	 * @return string
	 */
	public function getAuthorMail() {
		return $this->authorMail;
	}

	/**
	 * @param string $authorMail
	 */
	public function setAuthorMail($authorMail) {
		$this->authorMail = $authorMail;
	}

	/**
	 * Returns the score
	 *
	 * @return integer score 
	 */
	public function getScore() {
		return ($this->score ? $this->score : 0);
	}
	
	/**
	 * Sets the score
	 * 
	 * @param integer $score 
	 * @return void
	 */
	public function setScore($score) {
		$this->score = $score;
	}

	/**
	 * Returns the token
	 * 
	 * @return string $token 
	 */
	public function getToken() {
		return $this->token;
	}

	/**
	 * Sets the token
	 * 
	 * @param string $token
	 * @return void
	 */
	public function setToken($token) {
		$this->token = $token;
	}
	
	/**
	 *
	 * @return \DateTime $crdate
	 */
	public function getCrdate() {
		return $this->crdate;
	}

    /**
     * @param \DateTime $crdate
     */
    public function setCrdate($crdate) {
        $this->crdate = $crdate;
    }

	/**
	 * Get hidden
	 *
	 * @return boolean $hidden 
	 */
	public function getHidden() {
		return ($this->hidden ? TRUE : FALSE);
	}

	/**
	 * Set hidden
	 * 
	 * @param boolean $hidden 
	 * @return void
	 */
	public function setHidden($hidden) {
		$this->hidden = ($hidden ? TRUE : FALSE);
	}

	/**
	 * 
	 * @return boolean $deleted
	 */
	public function getDeleted() {
		return ($this->deleted ? TRUE : FALSE);
	}
	
	/**
	 * Set deleted
	 * 
	 * @param boolean $deleted
	 * @return void
	 */
	public function setDeleted ($deleted) {
		$this->deleted = ($deleted ? TRUE : FALSE);
	}

	/**
	 * Returns the score form the votes
	 * 
	 * @return integer $voteScore 
	 */
	public function getVoteScore() {
		$voteScore = 0;
		$votes = $this->getVotes();
		foreach ($votes as $vote) {
			$voteScore = $voteScore + $vote->getValue();
		}
		return $voteScore;
	}

	/**
	 * Adds a Vote
	 *
	 * @param Tx_T3seeTopics_Domain_Model_Vote $vote
	 * @return void
	 */
	public function addVote(Tx_T3seeTopics_Domain_Model_Vote $vote) {
		$this->votes->attach($vote);
	}

	/**
	 * Removes a Vote
	 *
	 * @param Tx_T3seeTopics_Domain_Model_Vote $voteToRemove The Vote to be removed
	 * @return void
	 */
	public function removeVote(Tx_T3seeTopics_Domain_Model_Vote $voteToRemove) {
		$this->votes->detach($voteToRemove);
	}

	/**
	 * Returns the votes
	 *
	 * @return Tx_Extbase_Persistence_ObjectStorage<Tx_T3seeTopics_Domain_Model_Vote> $votes
	 */
	public function getVotes() {
		return $this->votes;
	}

	/**
	 * Sets the votes
	 *
	 * @param Tx_Extbase_Persistence_ObjectStorage<Tx_T3seeTopics_Domain_Model_Vote> $votes
	 * @return void
	 */
	public function setVotes(Tx_Extbase_Persistence_ObjectStorage $votes) {
		$this->votes = $votes;
	}
	
	/**
	 * Get status voted
	 * 
	 * @return boolean $voted
	 */
	public function getVoted() {
		$ses = $GLOBALS['TSFE']->fe_user->getKey('ses','tx_t3seetopics');
		if (!is_array($ses['votedTopics'])){
			$ses['votedTopics'] = array();
		}
		if (in_array($this->uid, $ses['votedTopics'])){
			return TRUE;
		} else {
			return FALSE;
		}
		
	}

}
?>