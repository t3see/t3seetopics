<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2012 Jan <janYYYY@t3easy.de>
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/


/**
 *
 *
 * @package t3see_topics
 * @license http://www.gnu.org/licenses/lgpl.html GNU Lesser General Public License, version 3 or later
 *
 */
class Tx_T3seeTopics_Utility_TCAUserFunc {
	
	/**
	 * Set $params['title'] for vote
	 * @param array $params
	 * @param mixed $pObj
	 * @return void 
	 */
	public function getVoteLabel (&$params, &$pObj) {
		$vote = t3lib_BEfunc::getRecord($params['table'],$params['row']['uid']);
		$topic = t3lib_BEfunc::getRecord('tx_t3seetopics_domain_model_topic',$vote['topic'], 'title');
		$label = $topic['title'] . ' ' . sprintf('%+d', $vote['value']);
		$params['title'] = $label;
	}

}

?>
